package com.kantefier.ordermatchingtest

import org.scalatest.{FreeSpec, Matchers}
import com.kantefier.ordermatching.{Order, TradeOp}
import org.scalatest.prop.PropertyChecks
import org.scalacheck.Gen


class OrderTest extends FreeSpec with Matchers with PropertyChecks {
  import OrderTest._

  "Hashes of two Orders" - {
    "must be equal if asset, price and amount fields are equal" in {
      /**
        * Generates two random Orders with asset, price and amount fields equal
        */
      val genCorrespondingOrders: Gen[(Order, Order)] = orderGen
        .flatMap { o1 =>
          orderGen.map { o2 =>
            o1 -> o2.copy(asset = o1.asset, price = o1.price, amount = o1.amount)
          }
        }
      forAll(genCorrespondingOrders) { case (o1, o2) =>
        assert(o1.hash === o2.hash)
      }
    }

    "shouldn't match if one of (asset, price, amount) parameters isn't equal" in {
      forAll(orderGen, orderGen) { (o1, o2) =>
        whenever(o1.asset != o2.asset || o1.price != o2.price || o1.amount != o2.amount) {
          assert(o1.hash !== o2.hash)
        }
      }
    }
  }
  "parseFromString method" - {
    "should work for a proper record format (User TradeOp Asset Price Amount)" in {
      forAll(orderTxtRecord) { rec =>
        Order.parseFromString(rec)
      }
    }
  }
}

object OrderTest {
  /**
    * Generate a proper Order object
    */
  val orderGen: Gen[Order] = for {
    name <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
    tradeOp <- Gen.oneOf(TradeOp.Buy, TradeOp.Sell)
    assetName <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
    assetPrice <- Gen.choose(1, Int.MaxValue)
    assetAmount <- Gen.choose(1, Int.MaxValue)
  } yield Order(name, tradeOp, assetName, assetPrice, assetAmount)

  /**
    * Generator for a String record from "orders.txt"
    */
  val orderTxtRecord: Gen[String] = for {
    name <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
    tradeOp <- Gen.oneOf(TradeOp.Buy, TradeOp.Sell)
    assetName <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
    assetPrice <- Gen.choose(1, Int.MaxValue)
    assetAmount <- Gen.choose(1, Int.MaxValue)
  } yield name :: tradeOp.str :: assetName :: assetPrice :: assetAmount :: Nil mkString "\t"
}