package com.kantefier.ordermatchingtest

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.PropertyChecks
import com.kantefier.ordermatching.DeltaForAsset
import org.scalacheck.Gen


class DeltaForAssetTest extends FreeSpec with Matchers with PropertyChecks {
  import DeltaForAssetTest._

  "Object construction helper methods" - {
    "for buyer (gains assets, looses money)" in {
      forAll(Gen.choose(1, 10000), Gen.choose(1, 10000)) { (assetAmount, dollarDelta) =>
        val delta = DeltaForAsset.forBuyer(assetAmount, dollarDelta)
        assert(delta.assetAmount === assetAmount)
        assert(delta.dollarDelta === -dollarDelta)
      }
    }
    "for seller (looses assets, gains money)" in {
      forAll(Gen.choose(1, 10000), Gen.choose(1, 10000)) { (assetAmount, dollarDelta) =>
        val delta = DeltaForAsset.forSeller(assetAmount, dollarDelta)
        assert(delta.assetAmount === -assetAmount)
        assert(delta.dollarDelta === dollarDelta)
      }
    }
  }
  "Merging" in {
    forAll(deltaGen, deltaGen) { (d1, d2) =>
      val d3 = d1.merge(d2)
      assert(d3.dollarDelta === d1.dollarDelta + d2.dollarDelta)
      assert(d3.assetAmount === d1.assetAmount + d2.assetAmount)
    }
  }
}
object DeltaForAssetTest {
  /**
    * Generates DeltaForAsset object
    */
  val deltaGen: Gen[DeltaForAsset] = for {
    assetAmount <- Gen.choose(1, 10000)
    dollarDelta <- Gen.choose(1, 10000)
  } yield DeltaForAsset(assetAmount, dollarDelta)
}

