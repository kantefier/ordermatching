package com.kantefier.ordermatchingtest

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.PropertyChecks
import com.kantefier.ordermatching.Matching.processRecur
import com.kantefier.ordermatching.{Order, TradeOp}


class ProcessTest extends FreeSpec with Matchers with PropertyChecks {
  "Recursive process" - {
    "one buying, other selling" in {
      val orders =
        Order("C1", TradeOp.Buy, "A", 13, 11) ::
        Order("C2", TradeOp.Sell, "A", 13, 11) :: Nil

      val deltas = processRecur(orders.iterator)

      val d1 = deltas.get("C1", "A").get
      val d2 = deltas.get("C2", "A").get

      assert(d1.assetAmount === 11)
      assert(d1.dollarDelta === -13*11)
      assert(d2.assetAmount === -11)
      assert(d2.dollarDelta === 13*11)
    }
    "two buying, one selling" in {
      val orders =
        Order("C1", TradeOp.Buy, "A", 13, 11) ::
        Order("C2", TradeOp.Buy, "A", 13, 11) ::
        Order("C3", TradeOp.Sell, "A", 13, 11) :: Nil

      val deltas = processRecur(orders.iterator)

      val d1 = deltas.get("C1", "A").get
      val d3 = deltas.get("C3", "A").get

      assert(d1.assetAmount === 11)
      assert(d1.dollarDelta === -11 * 13)
      assert(deltas.get("C2", "A").isEmpty)
      assert(d3.assetAmount === -11)
      assert(d3.dollarDelta === 11 * 13)
    }
    "two selling, one buying" in {
      val orders =
        Order("C1", TradeOp.Sell, "A", 13, 11) ::
        Order("C2", TradeOp.Sell, "A", 13, 11) ::
        Order("C3", TradeOp.Buy, "A", 13, 11) :: Nil

      val deltas = processRecur(orders.iterator)

      val d1 = deltas.get("C1", "A").get
      val d3 = deltas.get("C3", "A").get

      assert(d1.assetAmount === -11)
      assert(d1.dollarDelta === 11 * 13)
      assert(deltas.get("C2", "A").isEmpty)
      assert(d3.assetAmount === 11)
      assert(d3.dollarDelta === -11 * 13)
    }
    "self trading" in {
      val orders =
        Order("C1", TradeOp.Sell, "A", 13, 11) ::
        Order("C1", TradeOp.Buy, "A", 13, 11) :: Nil

      val deltas = processRecur(orders.iterator)

      assert(deltas.isEmpty)
    }
    "no deals" in {
      val orders =
        Order("C1", TradeOp.Sell, "A", 13, 11) ::
        Order("C2", TradeOp.Sell, "A", 13, 11) :: Nil

      val deltas = processRecur(orders.iterator)

      assert(deltas.isEmpty)
    }
  }
}
