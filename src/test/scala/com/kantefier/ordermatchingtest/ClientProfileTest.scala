package com.kantefier.ordermatchingtest

import org.scalatest.{FreeSpec, Matchers}
import com.kantefier.ordermatching.{ClientProfile, DeltaForAsset}
import org.scalatest.prop.PropertyChecks
import org.scalacheck.Gen


class ClientProfileTest extends FreeSpec with Matchers with PropertyChecks {
  import ClientProfileTest._

  "Parsing" - {
    "parseFromString parses from a client record formatted String" in {
      forAll(clientRecordGen) { record =>
        ClientProfile.parseFromString(record)
      }
    }
    "parseFromString parses a record, that is made by toString method" in {
      forAll(clientGen) { client =>
        ClientProfile.parseFromString(client.toString)
      }
    }
  }
  "updateWithDelta method" - {
    "given a map of deltas updates the profile" in {
      forAll(clientGen, deltaMapGen) { (client, assetDelta) =>
        val dollarExpected = client.dollarBal + assetDelta.values.map(_.dollarDelta).sum
        val aAssetExpected = client.aAssetBal + assetDelta.get("A").map(_.assetAmount).getOrElse(0)
        val bAssetExpected = client.bAssetBal + assetDelta.get("B").map(_.assetAmount).getOrElse(0)
        val cAssetExpected = client.cAssetBal + assetDelta.get("C").map(_.assetAmount).getOrElse(0)
        val dAssetExpected = client.dAssetBal + assetDelta.get("D").map(_.assetAmount).getOrElse(0)
        val updatedClient = client.updateWithDelta(assetDelta)

        assert(updatedClient.dollarBal === dollarExpected)
        assert(updatedClient.aAssetBal === aAssetExpected)
        assert(updatedClient.bAssetBal === bAssetExpected)
        assert(updatedClient.cAssetBal === cAssetExpected)
        assert(updatedClient.dAssetBal === dAssetExpected)
      }
    }
  }
}

object ClientProfileTest {
  /**
    * Generate arbitrary ClientProfile object
    */
  val clientGen: Gen[ClientProfile] = for {
    name <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
    dollarBal <- Gen.choose(1, 10000)
    aAssetBal <- Gen.choose(1, 10000)
    bAssetBal <- Gen.choose(1, 10000)
    cAssetBal <- Gen.choose(1, 10000)
    dAssetBal <- Gen.choose(1, 10000)
  } yield ClientProfile(name, dollarBal, aAssetBal, bAssetBal, cAssetBal, dAssetBal)

  /**
    * Generate a String in format of "clients.txt" file
    */
  val clientRecordGen: Gen[String] = for {
    name <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
    dollarBal <- Gen.choose(1, 10000)
    aAssetBal <- Gen.choose(1, 10000)
    bAssetBal <- Gen.choose(1, 10000)
    cAssetBal <- Gen.choose(1, 10000)
    dAssetBal <- Gen.choose(1, 10000)
  } yield name :: dollarBal :: aAssetBal :: bAssetBal :: cAssetBal :: dAssetBal :: Nil mkString "\t"

  val deltaMapGen: Gen[Map[String, DeltaForAsset]] =
    Gen.mapOf {
      for {
        asset <- Gen.oneOf("A", "B", "C", "D")
        amount <- Gen.choose(1, 10000)
        dollarDelta <- Gen.choose(1, 10000)
      } yield asset -> DeltaForAsset(amount, dollarDelta)
    }
}
