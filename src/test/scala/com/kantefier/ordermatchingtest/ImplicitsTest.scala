package com.kantefier.ordermatchingtest

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.PropertyChecks
import com.kantefier.ordermatching._
import org.scalacheck.Gen
import scala.collection.immutable

class ImplicitsTest extends FreeSpec with Matchers with PropertyChecks {
  import ImplicitsTest._

  "ClientSubtotals implicit extension" - {
    "merge with empty" in {
      forAll(subtotalsGen) { subtotals =>
        val mergeResult = subtotals.merge(immutable.HashMap.empty)
        assert(subtotals === mergeResult)
      }
    }
    "merge with self" in {
      forAll(subtotalsGen) { subtotals =>
        val mergeResult = subtotals.merge(subtotals)
        mergeResult.foreach { case (mapKey, delta) =>
          val oldDelta = subtotals(mapKey)
          assert(delta.dollarDelta === oldDelta.dollarDelta * 2)
          assert(delta.assetAmount === oldDelta.assetAmount * 2)
        }
      }
    }
    "shorthand getter" in {
      forAll(subtotalsGen, subtotalKeyGen) { (subtotals, sKey) =>
        assert(subtotals.get(sKey) === subtotals.get(sKey.clientName, sKey.asset))
      }
    }
  }
  "ActiveOrders implicit extension" - {
    "UpdateOrClear" - {
      "empty queue for non-existing key" in {
        forAll(activeOrdersGen, genOrderKey) { (activeOrders, arbKey) =>
          whenever(!activeOrders.isDefinedAt(arbKey)) {
            val updatedOrders = activeOrders.updateOrClear(arbKey, immutable.Queue.empty)
            assert(updatedOrders.isDefinedAt(arbKey) === false)
          }
        }
      }
      "empty queue for existing key" in {
        forAll(activeOrdersGen) { activeOrders =>
          whenever(activeOrders.nonEmpty) {
            val presentKey = activeOrders.keys.head
            val updatedOrders = activeOrders.updateOrClear(presentKey, immutable.Queue.empty)
            assert(updatedOrders.isDefinedAt(presentKey) === false)
          }
        }
      }
      "queue for non-existing key" in {
        forAll(activeOrdersGen, OrderTest.orderGen) { (activeOrders, order) =>
          val orderKey = (order.hash, order.operation)
          whenever(!activeOrders.isDefinedAt(orderKey)) {
            val updatedOrders = activeOrders.updateOrClear(orderKey, immutable.Queue(order))
            assert(updatedOrders.isDefinedAt(orderKey))
          }
        }
      }
    }
    "AddToQueue" - {
      "adds order for non-existing key" in {
        forAll(activeOrdersGen, OrderTest.orderGen) { (activeOrders, order) =>
          val orderKey = (order.hash, order.operation)
          whenever(!activeOrders.isDefinedAt(orderKey)) {
            val updatedOrders = activeOrders.addToQueue(orderKey, order)
            assert(updatedOrders.isDefinedAt(orderKey))
            assert(updatedOrders(orderKey).dequeue._1 === order)
          }
        }
      }
      "adds order for existing key" in {
        forAll(activeOrdersGen, OrderTest.orderGen) { (activeOrders, order) =>
          val orderKey = (order.hash, order.operation)
          val preparedOrders = activeOrders.updateOrClear(orderKey, immutable.Queue(order))
          val updatedOrders = preparedOrders.addToQueue(orderKey, order)

          val queue = updatedOrders(orderKey)
          assert(queue.length === 2)
          queue.foreach(queuedOrder => assert(queuedOrder === order))
        }
      }
    }
  }


}

object ImplicitsTest {
  /**
    * Generate a key for subtotals map
    */
  val subtotalKeyGen: Gen[SubtotalKey] = for {
    client <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
    asset <- Gen.nonEmptyListOf(Gen.alphaNumChar).map(_.mkString)
  } yield SubtotalKey(client, asset)

  /**
    * Generates subtotals map
    */
  val subtotalsGen: Gen[ClientSubtotals] =
    Gen.nonEmptyListOf(
      for {
        key <- subtotalKeyGen
        delta <- DeltaForAssetTest.deltaGen
      } yield key -> delta)
      .map(lst => immutable.HashMap.apply(lst: _*))

  /**
    * Generate an arbitrary OrderKey
    */
  val genOrderKey: Gen[OrderKey] = for {
    arbNum <- Gen.choose(Int.MinValue, Int.MaxValue)
    arbOp <- Gen.oneOf(TradeOp.Buy, TradeOp.Sell)
  } yield arbNum -> arbOp

  /**
    * Generates arbitrary ActiveOrders
    */
  val activeOrdersGen: Gen[ActiveOrders] = {
    // generate the first order
    val singleRecord: Gen[(OrderKey, immutable.Queue[Order])] =
      OrderTest.orderGen.flatMap { order =>
        val orderKey = (order.hash, order.operation)
        val genSpecificOrder = OrderTest.orderGen.map(_.copy(asset = order.asset, amount = order.amount, price = order.price, operation = order.operation))
        // generate random number of Orders and replace their (asset, amount, price) with given
        val orderList = Gen.choose(1, 100).flatMap { queueCount =>
          Gen.listOfN(queueCount, genSpecificOrder)
        }
        // construct a tuple, turning a list of orders into a queue
        orderList.map { orders =>
          orderKey -> immutable.Queue.apply(orders: _*)
        }
      }

    Gen.choose(1, 100).flatMap { recordCount =>
      Gen.listOfN(recordCount, singleRecord).map { lst =>
        immutable.HashMap.apply(lst: _*)
      }
    }
  }
}