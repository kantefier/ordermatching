package com.kantefier.ordermatching


/**
  * Represents client profile from "clients.txt"
  */
case class ClientProfile(name: String, dollarBal: Int, aAssetBal: Int, bAssetBal: Int, cAssetBal: Int, dAssetBal: Int) { self =>
  /**
    * Takes delta aka subtotals as a map of (AssetName -> Delta) and creates updated profile
    */
  def updateWithDelta(assetToDelta: Map[String, DeltaForAsset]): ClientProfile = {
    ClientProfile.apply(
      name = self.name,
      dollarBal = self.dollarBal + assetToDelta.values.map(_.dollarDelta).sum,
      aAssetBal = self.aAssetBal + assetToDelta.get("A").map(_.assetAmount).getOrElse(0),
      bAssetBal = self.bAssetBal + assetToDelta.get("B").map(_.assetAmount).getOrElse(0),
      cAssetBal = self.cAssetBal + assetToDelta.get("C").map(_.assetAmount).getOrElse(0),
      dAssetBal = self.dAssetBal + assetToDelta.get("D").map(_.assetAmount).getOrElse(0)
    )
  }

  /**
    * Output file format
    */
  override def toString: String = {
    name ::
      dollarBal ::
      aAssetBal ::
      bAssetBal ::
      cAssetBal ::
      dAssetBal :: Nil mkString "\t"
  }
}

object ClientProfile {
  def parseFromString(str: String): ClientProfile = {
    val tokens = str.split('\t')
    ClientProfile(tokens(0), tokens(1).toInt, tokens(2).toInt, tokens(3).toInt, tokens(4).toInt, tokens(5).toInt)
  }
}