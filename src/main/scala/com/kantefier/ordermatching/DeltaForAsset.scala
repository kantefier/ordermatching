package com.kantefier.ordermatching

/**
  * Holds delta asset amount as well as dollar delta for all deals with corresponding asset
  */
case class DeltaForAsset(assetAmount: Int, dollarDelta: Int) { self =>
  /**
    * Sum deltas
    */
  def merge(delta: DeltaForAsset) =
    DeltaForAsset(assetAmount + delta.assetAmount, dollarDelta + delta.dollarDelta)
}

object DeltaForAsset {
  /**
    * Seller gives assets away for dollars
    */
  def forSeller(amount: Int, cost: Int) = DeltaForAsset(-amount, cost)

  /**
    * Buyer gives dollars for assets
    */
  def forBuyer(amount: Int, cost: Int) = DeltaForAsset(amount, -cost)
}