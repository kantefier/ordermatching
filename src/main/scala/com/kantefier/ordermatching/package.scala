package com.kantefier

import scala.collection.immutable


package object ordermatching {
  type Hash = Int
  type OrderKey = (Hash, TradeOp)
  type ClientSubtotals = immutable.HashMap[SubtotalKey, DeltaForAsset]
  type ActiveOrders = immutable.HashMap[OrderKey, immutable.Queue[Order]]


  /**
    * A search and store key for deltas in ClientSubtotals
    */
  case class SubtotalKey(clientName: String, asset: String)


  /**
    * A trading operation pseudo-enum: Sell or Buy
    */
  sealed trait TradeOp {
    def invert: TradeOp
    def str: String
  }
  object TradeOp {
    case object Buy extends TradeOp {
      def invert: TradeOp = TradeOp.Sell
      def str = "b"
    }
    case object Sell extends TradeOp {
      def invert: TradeOp = TradeOp.Buy
      def str = "b"
    }
  }


  /**
    * Implicit class with helper methods for ClientSubtotals
    */
  implicit class ClientSubtotals_implicit(m: ClientSubtotals) {
    /**
      * Merge two subtotals Maps into one
      */
    def merge(otherMap: ClientSubtotals): ClientSubtotals = {
      otherMap.foldLeft(m)((oldMap, record) => oldMap.mergeRecord(record))
    }

    /**
      * Merge a record. Merges deltas if found for the same key, otherwise adds to a Map
      */
    def mergeRecord(record: (SubtotalKey, DeltaForAsset)): ClientSubtotals = {
      val (key, delta) = record
      m.get(key) match {
        case Some(subtotal) =>
          m.updated(key, subtotal.merge(delta))
        case None =>
          m + record
      }
    }

    /**
      * Shorthand method
      */
    def get(clientName: String, assetName: String): Option[DeltaForAsset] =
      m.get(SubtotalKey(clientName, assetName))
  }


  /**
    * Implicit helper methods for ActiveOrders
    */
  implicit class ActiveOrders_implicit(m: ActiveOrders) {
    /**
      * Updates the Map with given queue. If it's empty, removes the key.
      */
    def updateOrClear(key: OrderKey, queue: immutable.Queue[Order]): ActiveOrders = {
      if(queue.nonEmpty)
        m.updated(key, queue)
      else
        m - key
    }

    /**
      * Adds Order to a queue for given key. Creates a Queue if there's none for the key.
      */
    def addToQueue(key: OrderKey, order: Order): ActiveOrders = {
      val queue = m.get(key) match {
        case Some(orderQueue) =>
          orderQueue.enqueue(order)
        case None =>
          immutable.Queue(order)
      }
      m.updated(key, queue)
    }
  }
}
