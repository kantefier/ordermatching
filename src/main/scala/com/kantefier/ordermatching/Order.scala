package com.kantefier.ordermatching


/**
  * Represents trade order (from "orders.txt")
  */
case class Order(clientName: String, operation: TradeOp, asset: String, price: Int, amount: Int) {
  /**
    * Hash to ease the search
    * Corresponding orders should have the same hash (asset, price and amount are equal), but inverse TradeOp's
    */
  lazy val hash: Hash =
    (asset :: price :: amount :: Nil).mkString("_").##
}

object Order {
  def parseFromString(str: String): Order = {
    val tokens = str.split('\t')
    val operation = tokens(1) match {
      case "b" => TradeOp.Buy
      case "s" => TradeOp.Sell
    }
    Order(tokens(0), operation, tokens(2), tokens(3).toInt, tokens(4).toInt)
  }
}