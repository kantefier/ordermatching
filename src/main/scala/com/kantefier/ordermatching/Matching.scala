package com.kantefier.ordermatching

import scala.collection._
import scala.io.Source
import java.io.File
import java.nio.file.{Files, Paths, StandardOpenOption}
import scala.annotation.tailrec

object Matching {
  def main(args: Array[String]): Unit = {
    // create iterator for "orders.txt"
    val ordersIter: Iterator[Order] = Source.fromFile(new File("orders.txt")).getLines().map(Order.parseFromString)
    // launch recursive process, get back deltas for client profiles
    val resultingDeltas = processRecur(ordersIter)

    // merge deltas with profiles from "clients.txt"
    val assets: List[String] = "A" :: "B" :: "C" :: "D" :: Nil
    val profiles = Source.fromFile(new File("clients.txt")).getLines().map(ClientProfile.parseFromString).toList
    val updatedProfiles = profiles.map { profile =>
      val assetDeltas =
        assets
          .flatMap(assetName =>
            resultingDeltas.get(profile.name, assetName).map(delta => assetName -> delta).toList)
          .toMap

      profile.updateWithDelta(assetDeltas)
    }

    // finally, write to file
    val profileBytes = updatedProfiles.mkString("\n").getBytes("UTF-8")
    Files.write(Paths.get("results.txt"), profileBytes, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)
  }


  /**
    * Recursive processing of Iterator[Order]
    *
    * 1. An Order is read from Iterator;
    * 2. Search activeOrders for corresponding order to make a deal;
    *   2.1 If one is found, a deal is made, resulting in subtotal/deltas for client profiles;
    *   2.2 Else an Order is persisted in a corresponding queue;
    * 3. Once an Iterator is exhausted, all Orders are then processed, so activeOrders queue could be dropped and clientSubtotals returned
    */
  @tailrec
  def processRecur(
            it: Iterator[Order],
            clientSubtotals: ClientSubtotals = immutable.HashMap.empty,
            activeOrders: ActiveOrders = immutable.HashMap.empty): ClientSubtotals = {
    if(it.hasNext) {
      val order = it.next()
      val orderKey = (order.hash, order.operation)
      val searchKey: OrderKey = (order.hash, order.operation.invert)

      activeOrders.get(searchKey) match {

        case Some(orderQueue) if orderQueue.nonEmpty =>
          // take the first one from waiting queue
          val (suitableOrder, restOrders) = orderQueue.dequeue
          // deal results
          val deltas = order.operation match {
            case TradeOp.Buy  => deltasForDeal(sellingOrder = suitableOrder, buyingOrder = order)
            case TradeOp.Sell => deltasForDeal(sellingOrder = order, buyingOrder = suitableOrder)
          }
          val subtotals = clientSubtotals.merge(deltas)
          processRecur(it, subtotals, activeOrders.updateOrClear(searchKey, restOrders))


        case _ =>
          // no suitable found, add to waiting list
          processRecur(it, clientSubtotals, activeOrders.addToQueue(orderKey, order))
      }
    } else clientSubtotals
  }


  /**
    * Takes two orders and returns client deltas as a result of a deal
    * For self trading case no delta is generated
    */
  def deltasForDeal(sellingOrder: Order, buyingOrder: Order): ClientSubtotals = {
    if(sellingOrder.clientName != buyingOrder.clientName) {
      val orderCost: Int = sellingOrder.amount * sellingOrder.price
      val amount = sellingOrder.amount
      val sellerDelta = SubtotalKey(sellingOrder.clientName, sellingOrder.asset) -> DeltaForAsset.forSeller(amount, orderCost)
      val buyerDelta = SubtotalKey(buyingOrder.clientName, buyingOrder.asset) -> DeltaForAsset.forBuyer(amount, orderCost)
      immutable.HashMap(sellerDelta, buyerDelta)
    } else immutable.HashMap.empty
  }
}