name := "Matching"

organization := "com.kantefier"

version := "0.0.1"

lazy val root = (project in file("."))
  .settings(
    scalaVersion := "2.12.4",
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.0.4" % "test",
      "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"
      )
    )
  